; LENS operating system kernel copyright (C) 2018 Alexander Martin.

; This file is part of LENS.

; LENS is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; LENS is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with LENS.  If not, see <http://www.gnu.org/licenses/>.

; Multiboot header constants.
MBALIGN     equ  1<<0                   ; Align loaded modules on page boundaries.
MEMINFO     equ  1<<1                   ; Provide memory map.
GRAPHICS    equ  1<<2                   ; Set graphics mode.
FLAGS       equ  MBALIGN | MEMINFO | GRAPHICS ; Multiboot 'flag' field.
MAGIC       equ  0x1BADB002             ; Magic number.
CHECKSUM    equ -(MAGIC + FLAGS)        ; Checksum.

; Declare multiboot header.
section .mbheader

global multibootHeader

multibootHeader:
align 4
    dd MAGIC
    dd FLAGS
    dd CHECKSUM
    dd 0 ; Fields for a.out kludge are zero.
    dd 0
    dd 0
    dd 0
    dd 0
    dd 1 ; Tell bootloader we want 80x25 text mode.
    dd 80
    dd 25
    dd 0
