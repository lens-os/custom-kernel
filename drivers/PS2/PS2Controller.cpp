// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PS2Controller.h"
#include <kernel.h>
#include <util/util.h>
#define DEBUG 1
#include <debug.h>


void PS2Controller::outB(uint16_t offset, char data) {
  while (! this->inputBufferIsEmpty()) {
    this->range.ioWait();
  }
  this->range.outB(offset, data);
}


bool PS2Controller::sendByteToDevice(char toSend, int device) {
  if (this->singleChannel && device == 1) { // Single-channel controllers can't send bytes to the second device.
    return false;
  }

  if (device == 1) {
    this->outB(0x0004, 0xD4); // Select second device for next byte sent.
  }

  // TODO: Set up a timeout.

  while (! this->inputBufferIsEmpty()) { // Wait for the buffer to be empty.
    /*
      if (<timeout expired>) { // Fail if the timeout expires.
        return false
      }
    */
  }

  this->outB(0x0000, toSend);

  return true;
}

char PS2Controller::getByteFromDevice(int device) {
  UNUSED_PARAM(device);
  panic("Call to unimplemented member function PS2Controller::getByteFromDevice(int device).");
  // TODO: Switch this to fetching the byte from an internal buffer populated by IRQs.
  // WARN: THIS IMPLEMENTATION CAN'T TELL THE DIFFERENCE BETWEEN TWO PORTS!
  //return this->range.inB(0x0000);
  return 0x00;
}

bool PS2Controller::outputBufferIsEmpty() {
  char statusReg = this->range.inB(0x0004); // Get the status register.
  statusReg = statusReg & 0x01; // Mask off all but bit 0.
  return (! (bool) statusReg); // Return the result as a bool, which'll be bit 0 (output buffer full) notted.
}

bool PS2Controller::inputBufferIsEmpty() {
  char statusReg = this->range.inB(0x0004); // Get the status register.
  statusReg = statusReg & 0x02; // Mask off all but bit 1.
  return (bool) statusReg; // Return the result as a bool, which'll be bit 1 (input buffer empty).
}

void PS2Controller::flushOutputBuffer() {
  while (!this->outputBufferIsEmpty()) {
    this->range.inB(0x0000);
  }
}


void PS2Controller::resetSystem() {
  this->outB(0x0004, 0xFE);
  panic("Reset failed.");
}


bool PS2Controller::start() {
  printk("Initializing PS/2 controller... ");
  this->outB(0x0004, 0xAD); // Disable all PS/2 devices. This keeps them from interfering with initialisation.
  this->outB(0x0004, 0xA7);

  this->range.ioWait();

  this->flushOutputBuffer();

  this->outB(0x0004, 0x20); // Get the config byte.
  char config = this->range.inB(0x0000);
  config = config & (0x01 | 0x02 | 0x40); // Clear bits 0, 1, and 6 of the config byte (disable all interrupts and translation).
  this->outB(0x0004, 0x60); // Write back the modified config byte.
  this->outB(0x0000, config);

  this->range.ioWait();


  this->singleChannel = true;
  // TODO: Test for single-channel controllers.

  this->outB(0x0004, 0xAA); // Do a controller self-test.
  if (this->range.inB(0x0000) != 0x55) {
    printk("[ FAILED ] (controller self-test did not pass)\n");
    return false;
  }

  this->range.ioWait();

  // TODO: Test for single-channel controllers.

  this->outB(0x0004, 0xAB); // Test first PS/2 port.
  if (this->range.inB(0x00) != 0x00) {
    printk("[ FAILED ] (first port test did not pass)\n");
    return false;
  }
  if (! this->singleChannel) {
    this->outB(0x0004, 0xA9); // Test second PS/2 port.
    if (this->range.inB(0x00) != 0x00) {
      printk("[ FAILED ] (second port test did not pass)\n");
      return false;
    }
  }

  this->range.ioWait();

  this->outB(0x0004, 0xAE); // Enable both devices.
  if (! this->singleChannel) {
    this->outB(0x0004, 0xA8);
  }
  // TODO: Enable IRQs.

  this->range.ioWait();

  this->sendByteToDevice(0xFF, 0); // Send 0xFF to both devices.
  // TODO: Check for a failure.
  if (! this->singleChannel) {
    this->sendByteToDevice(0xFF, 1);
    // TODO: Check for a failure.
  }

  this->range.ioWait();

  printk("[ OK ]\n");
  return true;
}

bool PS2Controller::stop() {
  return false;
}

PS2Controller::PS2Controller() {
  this->range = IORange(0x0060); // Create the I/O range for the PS/2 controller (ports 0x60 and 0x64).
}
