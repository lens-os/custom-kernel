// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PS2DEVICE_H
#define PS2DEVICE_H

#include <drivers/abstract/Device.h>

/**
 * A PS/2 device.
 */
class PS2Device : Device {
protected:
  char generalType[16] = "PS2Device";
  char specificType[16] = "Generic";
public:
  int port; ///< The port the device is attached to.
  PS2Device();
};

#endif // PS2DEVICE_H
