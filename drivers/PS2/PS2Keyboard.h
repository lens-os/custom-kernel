// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PS2KEYBOARD_H
#define PS2KEYBOARD_H

#include "PS2Device.h"
#include <types.h>
#include <unicode/types.h>

enum KeyboardLed { // Identifiers for the keyboard LEDs.
  capsLock,
  numLock,
  scrollLock,
};

enum ModifierKey {
  shiftKey,
  controlKey,
  altKey,
  superKey,
};

typedef struct KeyboardPacket {
  bool capsLock : 1;
  bool numLock : 1;
  bool scrollLock : 1;
  bool shift : 1;
  bool control : 1;
  bool alt : 1;
  bool super : 1;
  Unicode::codepoint keyValue; // Special keys use Private Use Area code points.
} KeyboardPacket;

//#define DRIVER_PS2_KEYBOARD

/**
 * A PS/2 keyboard.
 */
class PS2Keyboard : PS2Device {
protected:
  char generalType[16] = "Keyboard";
  char specificType[16] = "PS2Keyboard";
public:
  unsigned char getKey(); // Returns the next key in the buffer, if there is one. Otherwise, returns '\0'.
  bool setLed(KeyboardLed led, bool state); // Sets the specified keyboard LED to the specified state. Returns true on success.
  bool getLed(KeyboardLed led); // Returns the state of the specified keyboard LED.
  bool checkModifierKey(ModifierKey key); // Returns the state (depressed or released) of the specified modifer key.
};

#endif // PS2KEYBOARD_H
