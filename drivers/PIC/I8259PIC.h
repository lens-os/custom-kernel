// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <io/IORange.h>
#include <drivers/abstract/InterruptController.h>

#ifndef I8259PIC_H
#define I8259PIC_H

class I8259PIC : public InterruptController {
private:
  IORange master;
  IORange slave;
protected:
  char specificType[16] = "I8259PIC";
public:
  bool mapIRQs(int base);
  bool maskIRQ(int index, bool enabled);
  void maskAllIRQs(bool enabled);
  bool getIRQMasked(int index);
  void endOfInterrupt(int index);
  bool checkSpuriousInterrupt(int index);
  bool checkInterruptValid(int index);
  bool start();
  bool stop();
  I8259PIC();
};

#endif // I8259PIC_H
