// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "defaultFont.h"
#include "VGAController.h"
#include "VGATextDisplay.h"

/**
 * Creates the appropriate VGADisplay for the current display mode.
 *
 * @param mode The mode to create the display for.
 *
 * @return The result of starting the display driver.
 */
bool VGAController::createDisplay(VGAMode mode) {
  if (this->getFirstChild() != NULL) {
    this->getFirstChild()->stop();
    delete this->getFirstChild();
  }
  this->mode = mode;
  switch (mode) {
    case text80x25:
    this->firstChild = new VGATextDisplay(80, 25);
    ((VGATextDisplay *) this->getFirstChild())->setFont(getDefaultFont());
    return this->getFirstChild()->start();
    break;
    default:
    return false;
    break;
  }
}

bool VGAController::setMode(VGAMode mode) {
  switch (mode) {
    case text80x25:
    // TODO: Configure VGA hardware.
    break;
    // TODO: Add more modes.
    default:
    return false;
    break;
  }
  return this->createDisplay(mode);
}

VGAMode VGAController::getMode() {
  return this->mode;
}

bool VGAController::start() {
  return this->createDisplay(text80x25);
}

bool VGAController::stop() {
  return this->getFirstChild()->stop();
}

VGAController::VGAController() {
  this->range = IORange(0x3C0);
}
