// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <types.h>
#include <drivers/abstract/Device.h>
#include <io/IORange.h>

#ifndef VGACONTROLLER_H
#define VGACONTROLLER_H

typedef enum VGAMode {
  text80x25,
} VGAMode;

/**
 * The VGA graphics controller.
 *
 * Manages mode switching and the VGA display.
 */
class VGAController : public Device {
private:
  IORange range;
  VGAMode mode;
protected:
  char generalType[16] = "VGAController";
  char specificType[16] = "Generic";
public:
  bool createDisplay(VGAMode mode);
  bool setMode(VGAMode mode);
  VGAMode getMode();
  bool start();
  bool stop();
  VGAController();
};

#endif // VGACONTROLLER_H
