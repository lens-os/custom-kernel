// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "VGATextDisplay.h"
#include <util/util.h>
#include <util/strutil.h>
#include <unicode/utf8.h>

void VGATextDisplay::updateCursor() {
  this->range.outB(0x14, 0x0F); // Select Cursor Location Low register.
  this->range.outB(0x15, (this->cursor & 0x00FF)); // & 00FF to mask off the high byte.
  this->range.outB(0x14, 0x0E); // Select Cursor Location High register.
  this->range.outB(0x15, ((this->cursor >> 8) & 0x00FF));
}

void VGATextDisplay::scroll() {
  for (size_t i = 1; i < (size_t) this->height; i++) { // Move all text up by one.
    memcpy((i * this->width * 2 + this->framebuffer), ((i - 1) * this->width * 2 + this->framebuffer), this->width * 2);
  }

  for (size_t off = (this->height - 1) * this->width * 2; off < (size_t) (this->width * this->height * 2); off++) { // Blank the last line.
    this->framebuffer[off] = 0;
  }

  this->cursor -= this->width; // Move the cursor back.
}

bool VGATextDisplay::setCursor(int x, int y) {
  if (x > this->width) { // Deal with cursor position overflow.
    y += x / this->width;
    x %= this->width;
  } else if (y > this->height) {
    return false;
  }
  this->cursor = (y * width + x);
  this->updateCursor();
  return true;
}

bool VGATextDisplay::setCursorMode(CursorMode mode) {
  bool disable;
  uint8_t cursorStart;
  uint8_t cursorEnd;

  switch(mode) { // Set up values to send to VGA.
    case disabled:
    disable = true;
    cursorStart = 0;
    cursorEnd = 0;
    break;
    case line:
    disable = false;
    cursorStart = 14;
    cursorEnd = 15;
    break;
    case block:
    disable = false;
    cursorStart = 0;
    cursorEnd = 15;
    break;
    default:
    return false;
    break;
  }

  this->range.outB(0x14, 0x0A); // Select Cursor Start register.
  this->range.outB(0x15, (char) (disable * 0x20) | (cursorStart & 0xE0)); // Put disable in bit 5 and cursorStart in bits 0-4.
  this->range.outB(0x14, 0x0B); // Select Cursor End register.
  this->range.outB(0x15, (char) cursorEnd & 0xE0);
  return true;
}

bool VGATextDisplay::setFont(Unicode::Font *font) { // Loads a font into the VGA display and uses its mappings.
  // TODO: Load glyphs into the font plane.
  this->font = font;
}

void VGATextDisplay::printGlyphs(char *str, Color fgColor, Color bgColor) {
  char attrib = fgColor | (bgColor << 4);
  for (int i = 0; str[i] != 0; i += 1) {
    if (str[i] == '\n') {
      this->cursor += this->width - this->cursor % this->width; // Skip to the next line.
    }
    if (this->cursor > this->width * this->height) { // Handle text overflowing off the bottom of the screen.
      this->scroll();
    }
    if (str[i] != '\n') {
      this->framebuffer[this->cursor * 2] = str[i];
      this->framebuffer[this->cursor * 2 + 1] = attrib;
      this->cursor++;
    }
  }
  this->updateCursor();
}

void VGATextDisplay::print(char *str, Color fgColor, Color bgColor) {
  size_t off = 0;
  size_t glfOff = 0;
  char *glyphString = new char[strlen(str)];
  while (str[off] != 0) { // Iterate over the string until we reach the end.
    Unicode::codepoint point = 0;
    int len = 0;
    if (str[off] == '\n') { // Handle newlines.
      glyphString[glfOff] = '\n';
      off++;
      glfOff++;
      continue;
    }
    if (Unicode::parseUtf8Character(str + off, &point, &len)) {
      glyphString[glfOff] = this->font->getGlyphNumForCodePoint(point);
      glfOff++; // Increase the offsets
      off += len;
    } else { // Bad character, ignore len.
      glyphString[glfOff] = this->font->getGlyphNumForCodePoint(0xFFFD); // Insert the Unicode Replacement Character (the empty square or diamond with question mark).
      glfOff++;
      off++;
    }
  }
  glyphString[glfOff] = 0; // Terminate the string.
  this->printGlyphs(glyphString, fgColor, bgColor);
  delete glyphString;
}

void VGATextDisplay::clear() {
  for (int i = 0; i < this->width * this->height * 2; i++) {
    this->framebuffer[i] = 0;
  }
  this->setCursor(0, 0);
}

bool VGATextDisplay::start() {
  this->clear();
  this->setCursorMode(line);
  return true;
}

bool VGATextDisplay::stop() {
  this->clear();
  return true;
}

VGATextDisplay::VGATextDisplay(int width, int height): width(width), height(height) {
  this->range = IORange(0x3C0);
}
