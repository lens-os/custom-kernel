// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DEVICE_H
#define DEVICE_H

#include <types.h>

/**
 * A hardware device in the device tree.
 *
 * May have children and a parent, and has a "general" and "specific" type.
 * Also has methods to start and stop the driver.
 */
class Device {
protected:
  char generalType[16] = "Unknown";
  char specificType[16] = "Generic";
  Device *parent = NULL;
  Device *nextSibling = NULL;
  Device *firstChild = NULL;
public:
  Device *getParent();
  bool setParent(Device *newParent);
  Device *getNextSibling(); // The children of a Device are kept in a linked list, which this method and setNextSibling() are responsible for managing.
  bool setNextSibling(Device *newSibling);
  Device *getFirstChild();
  char *getGeneralType(); // General and specific types. The general type is 'what API do I support', the specific 'what kind of physical device am I'.
  char *getSpecificType();
  /**
   * Starts the driver.
   * 
   * @return Whether starting was successful.
   */
  virtual bool start() = 0;
  /**
   * Stops the driver.
   *
   * @return Whether stopping was successful.
   */
  virtual bool stop() = 0;
};

#endif // DEVICE_H
