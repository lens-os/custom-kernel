// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SerialPort.h"

#define FLAG_LINE_STATUS_TRANSMIT_READY 0x20
#define FLAG_LINE_STATUS_RECEIVE_READY 0x01

bool SerialPort::setup() {
  this->range.outB(0x0001, 0x00);
  this->range.outB(0x0003, 0x80);
  this->range.outB(0x0000, (char) this->baud);
  this->range.outB(0x0001, (char) this->baud << 8);
  this->range.outB(0x0003, (char) this->encoding);
  this->range.outB(0x0002, 0xC7);
  return true;
}

bool SerialPort::write(char *data, size_t len) {
  if (! this->up) {
    return false;
  }
  for (size_t i = 0; i < len; i++) {
    while (! (this->range.inB(0x0005) & FLAG_LINE_STATUS_TRANSMIT_READY)) {
      this->range.ioWait();
    }
    this->range.outB(0x0000, data[i]);
  }
  return true;
}

bool SerialPort::read(char *buf) {
  if (! this->up) {
    return false;
  }
  while (! (this->range.inB(0x0005) & FLAG_LINE_STATUS_RECEIVE_READY)) {
    this->range.ioWait();
  }
  *buf = this->range.inB(0x0000);
  return true;
}

bool SerialPort::readMultiple(char *buf, size_t len) {
  if (! this->up) {
    return false;
  }
  for (size_t i = 0; i < len; i++) {
    this->read(buf + i);
  }
  return true;
}

bool SerialPort::setBaud(Baud rate) {
  this->baud = rate;
  return this->setup();
}

bool SerialPort::setEncoding(char data, char stop, char parity) {
  this->encoding = data | stop | parity;
  return this->setup();
}

bool SerialPort::start() {
  if (! this->valid) {
    return false;
  }
  this->up = true;
  return this->setup();
}

bool SerialPort::stop() {
  this->up = false;
  return true;
}

SerialPort::SerialPort(int portNumber) {
  this->portNumber = portNumber;
  uint16_t rangeBase;
  switch (portNumber) {
    case 1:
    rangeBase = 0x03F8;
    break;
    case 2:
    rangeBase = 0x02F8;
    break;
    case 3:
    rangeBase = 0x03E8;
    break;
    case 4:
    rangeBase = 0x02E8;
    break;
  }
  this->range = IORange(rangeBase);
  this->valid = true;
}

SerialPort::SerialPort() {
  this->valid = false;
}
