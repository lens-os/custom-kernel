// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef STRUTIL_H
#define STRUTIL_H

#include <types.h>
#include <printf/printf.h>

/**
 * @file util/strutil.h
 *
 * String utility functions.
 */

void strReverse(char str[], size_t length);
void printInt(long num, int base);
size_t strlen(const char *str);

#endif // STRUTIL_H
