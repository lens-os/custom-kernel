// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "util.h"
#include "strutil.h"
#include <kernel.h>
#include <buildinfo.h>
#include <drivers/drivers.h>

/**
 * @file util/util.cpp
 *
 * Various utility functions used across the kernel.
 */

void memcpy(void *from, void *to, size_t length) { // Copy length bytes.
  char *source = (char *) from;
  char *dest = (char *) to;
  // TODO: Make this work with overlapping source and destination.
  for (size_t i = 0; i < length; i++) {
    dest[i] = source[i];
  }
}

void printk(const char *message) {
  VGATextDisplay *display = (VGATextDisplay *) mainVGA.getFirstChild();
  display->print((char *) message, grey, black);
}

void printkColored(const char *message, Color fg, Color bg) {
  VGATextDisplay *display = (VGATextDisplay *) mainVGA.getFirstChild();
  display->print((char *) message, fg, bg);
}

void halt() {
  printk("System halted.\n");
  asm("cli; .haltloop: hlt; jmp .haltloop");
}

/**
 * Deals with a catastrophic failure.
 *
 * Halts the system after printing a register dump and reason message.
 *
 * @param reason The reason for the failure.
 */
void panic(const char *reason) {
  uint32_t eax32, ebx32, ecx32, edx32, esi32, edi32, ebp32, esp32;

  /*asm(
    "movl %%rax, %[a1] ;"
    "movl %%rbx, %[b1] ;"
    "movl %%rcx, %[c1] ;"
    "movl %%rdx, %[d1] ;"
    "movl %%rsi, %[si1] ;"
    "movl %%rdi, %[di1] ;"
    "movl %%rbp, %[bp1] ;"
    "movl %%rsp, %[sp1] ;"
    :
    [a1] "=m" (eax32), [b1] "=m" (ebx32), [c1] "=m" (ecx32), [d1] "=m" (edx32), [si1] "=m" (esi32), [di1] "=m" (edi32), [bp1] "=m" (ebp32), [sp1] "=m" (esp32)
  );*/

  VGATextDisplay *display = (VGATextDisplay *) mainVGA.getFirstChild();
  display->clear();

  printkColored("!!PANIC!!\n", brightRed, black);

  /*printf("eax=%x ebx=%x ecx=%x edx=%x\n", eax32, ebx32, ecx32, edx32);
  printf("esi=%x edi=%x ebp=%x esp=%x\n", esi32, edi32, ebp32, esp32);*/

  printk("Kernel panic: ");
  printk(reason);

  halt();
}

void printVersion() {
  printk("LENS kernel version ");
	printk(KERNEL_VERSION);
	printk(" for ");
	printk(KERNEL_PLATFORM);
	printk(", built ");
	printk(&__DATE__[0]);
	printk(" ");
	printk(&__TIME__[0]);
	printk(" by ");
	printk(COMPILER);
	printk(", version ");
	printk(COMPILER_VERSION);
	//printk("unknown");
	printk(".\n");
}
