// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef THREADSTATE_H
#define THREADSTATE_H

#include <types.h>

/**
 * @file util/threadstate.h
 *
 * Thread state information (register sets, stack position) to allow longjmp-like functionality and thread dispatch.
 */

typedef struct ThreadState { // Just all the registers, except RAX, becuase that gets clobbered by the return value.
  uint64_t rbx;
  uint64_t rcx;
  uint64_t rdx;
  uint64_t rbp;
  uint64_t rsp;
  uint64_t rsi;
  uint64_t rdi;
  uint64_t r8;
  uint64_t r9;
  uint64_t r10;
  uint64_t r11;
  uint64_t r12;
  uint64_t r13;
  uint64_t r14;
  uint64_t r15;
  uint64_t rip; // Return address.
} __attribute__((__packed__)) ThreadState;

extern "C" int saveState(ThreadState *stateBuf); // Saves all local state.
extern "C" void restoreState(ThreadState *state); // Restores all local state.

#endif // THREADSTATE_H
