# Automatically generate lists of sources using wildcards.
CXX_SOURCES = $(wildcard *.cpp drivers/*/*.cpp interrupts/*.cpp io/*.cpp kexcept/*.cpp mem/*.cpp printf/*.cpp sync/*.cpp unicode/*.cpp util/*.cpp)
HEADERS = $(wildcard *.h drivers/*.h drivers/*/*.h interrupts/*.h io/*.h kexcept/*.h mem/*.h printf/*.h sync/*.h unicode/*.h util/*.h)
ASM_SOURCES = $(wildcard *.asm util/*.asm)

# Convert the source filenames to object filenames to give a list of object files to build.
OBJ = ${ASM_SOURCES:.asm=.o} ${CXX_SOURCES:.cpp=.o}

CXXC = x86_64-elf-gcc
ASM = nasm
LD = x86_64-elf-gcc
STRIP = x86_64-elf-strip
OBJCOPY = x86_64-elf-objcopy

CXXFLAGS = -ffreestanding -fno-exceptions -fno-rtti -fno-asynchronous-unwind-tables -funsigned-char -fpie -std=gnu++11 -I. -Wall -Wextra -m64 -march=x86-64 -mgeneral-regs-only -mno-red-zone -g -c -O0
ASMFLAGS = -f elf64
LDFLAGS = -T klink.ld -nostdlib -lgcc -Xlinker --oformat -Xlinker elf64-x86-64

# Default build target.
default: build

rebuild: clean build

build: debug

production: stripped

debug: unstripped
	${OBJCOPY} --only-keep-debug "lenskernel" "lenskernel.debug"
	${STRIP} --strip-debug --strip-unneeded "lenskernel"
	${OBJCOPY} --add-gnu-debuglink="lenskernel.debug" "lenskernel"

stripped: lenskernel
	${STRIP} lenskernel

unstripped: lenskernel

# This builds the LENS Kernel image.
lenskernel: ${OBJ} klink.ld
	${LD} ${LDFLAGS} -o $@ ${OBJ}

# Generic rule to compile C++ (all C++ sources automatically depend on all headers).
%.o: %.cpp ${HEADERS}
	${CXXC} ${CXXFLAGS} $< -o $@

# Generic rule to assemble ASM.
%.o: %.asm
	${ASM} ${ASMFLAGS} $< -o $@

clean:
	rm -f lenskernel lenskernel.debug ${OBJ}

love:
	@echo "Not war?"
