// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "interruptmgr.h"
#include <mem/alloc.h>
#include <util/util.h>
#include <printf/printf.h>

extern void *gdt64_codeSegment;

namespace InterruptManager {

IDTDescriptor interrupts = {0, NULL};

struct _ISRBreak {
  uint16_t low16;
  uint16_t mid16;
  uint32_t high32;
} __attribute__((__packed__));

union ISRBreak {
  struct _ISRBreak broken;
  void *ptr;
};

__attribute__((interrupt)) void defaultFaultHandlerWithCode(struct interruptFrame *frame, uint64_t errorCode) {
  UNUSED_PARAM(errorCode);
  char buf[42];
  sprintf(&buf[0], "Unhandled fault at RIP=0x%lx.", frame->rip);
  panic(&buf[0]);
}

__attribute__((interrupt)) void defualtFaultHandler(struct interruptFrame *frame) {
  char buf[42];
  sprintf(&buf[0], "Unhandled fault at RIP=0x%lx.", frame->rip);
  panic(&buf[0]);
}

bool loadIDT(IDTDescriptor *IDTDesc) {
  if (interrupts.IDT != NULL) { // NULL can't be an IDT!
    asm("lidt (%0)" : :"r"(IDTDesc)); // Put IDTDesc in a register and lidt it.
    return true;
  } else {
    return false;
  }
}

/**
 * Puts an interrupt handler into the IDT.
 *
 * @param index The index of the interrupt to register a handler for.
 * @param handler The interrupt service routine (ISR) to register.
 * @param disableDuring If set, local interrupts will be disabled while the ISR runs.
 * @param user Allows the interrupt to be triggered from userspace.
 * @param stack The index of the alternate stack to switch to while handling. 0 for defualt stack.
 *
 * @return true on success, false if the interrupt manager has not been initialized.
 */
bool setInterruptHandler(int index, ISR handler, bool disableDuring, bool user, int stack) {
  if (interrupts.IDT != NULL) {
    union ISRBreak breaker;
    breaker.ptr = handler.ptr; // Break apart the ISR pointer.
    InterruptDescriptor *descriptor = &(interrupts.IDT[index]);

    bool prevEnabled = getLocalInterruptsEnabled();
    setLocalInterruptsEnabled(false); // Disable interrupts so things won't go wrong while we're setting up.

    descriptor->offset1 = breaker.broken.low16; // Populate pointer fields.
    descriptor->offset2 = breaker.broken.mid16;
    descriptor->offset3 = breaker.broken.high32;

    descriptor->selector = (uint16_t) (intptr_t) 0x08;

    descriptor->stack = (uint8_t) stack;

    uint8_t intType; // Set the flags.
    if (disableDuring) {
      intType = INTDESC_TYPE_INTERRUPT;
    } else {
      intType = INTDESC_TYPE_TRAP;
    }
    uint8_t intPriv;
    if (user) {
      intPriv = INTDESC_PRIV_USER;
    } else {
      intPriv = INTDESC_PRIV_KERNEL;
    }
    descriptor->flags = intType | intPriv | INTDESC_PRESENT;

    descriptor->zero = 0;

    setLocalInterruptsEnabled(prevEnabled);
    return true;
  }
  return false;
}

ISR getInterruptHandler(int index) {
  union ISRBreak breaker;
  InterruptDescriptor *descriptor = &(interrupts.IDT[index]);

  breaker.broken.low16 = descriptor->offset1;
  breaker.broken.mid16 = descriptor->offset2;
  breaker.broken.high32 = descriptor->offset3;

  ISR handler;
  handler.ptr = breaker.ptr;

  return handler;
}

bool removeInterruptHandler(int index) {
  InterruptDescriptor *descriptor = &(interrupts.IDT[index]);

  bool prevEnabled = getLocalInterruptsEnabled();
  setLocalInterruptsEnabled(false); // Disable interrupts so things won't go wrong while we're setting up.

  bool original = descriptor->flags & INTDESC_PRESENT; // Get the present flag.

  descriptor->flags = descriptor->flags & (!INTDESC_PRESENT); // Mask off the present flag.

  setLocalInterruptsEnabled(prevEnabled);
  return original;
}

bool getLocalInterruptsEnabled() {
  bool result;
  asm(
    "pushf;"
    "pop %w0;"
    "and $0x0200, %w0"
    : "=r" (result)
  );
  return result;
}

void setLocalInterruptsEnabled(bool enabled) {
  if (enabled) {
    asm("sti");
  } else {
    asm("cli");
  }
}

void init() {
  bool prevEnabled = getLocalInterruptsEnabled();
  setLocalInterruptsEnabled(false); // Disable interrupts so things won't go wrong while we're setting up.
  interrupts.IDT = (InterruptDescriptor *) malloc(sizeof(InterruptDescriptor) * 256); // Get 4k of memory for the IDT.
  interrupts.size = (sizeof(InterruptDescriptor) * 256) - 1;
  for (int i = 0; i < 256; i++) { // Zero all present bits so we don't fire nonexistent handlers.
    interrupts.IDT[i].flags = 0;
  }
  ISR defaultHnd;
  defaultHnd.intr = &defualtFaultHandler;
  ISR defaultHndCode;
  defaultHndCode.excp = &defaultFaultHandlerWithCode;
  for (int i = 0; i <= 0x20; i++) { // Set up default handlers for faults.
    switch (i) {
      case 0x08:
      case 0x0A:
      case 0x0B:
      case 0x0C:
      case 0x0D:
      case 0x0E:
      case 0x11:
      case 0x1E:
      setInterruptHandler(i, defaultHndCode, true, false, 0);
      break;
      default:
      setInterruptHandler(i, defaultHnd, true, false, 0);
    }
  }
  loadIDT(&interrupts);
  setLocalInterruptsEnabled(prevEnabled);
}

}
