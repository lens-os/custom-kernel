// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <types.h>

/**
 * @file io/absoluteio.h
 *
 * Absolute I/O functions.
 *
 * Not remotely portable.
 */

#ifndef ABS_IO_H
#define ABS_IO_H

char absoluteInB(uint16_t port);
void absoluteOutB(uint16_t port, char data);
uint16_t absoluteInW(uint16_t port);
void absoluteOutW(uint16_t port, uint16_t data);
uint32_t absoluteInL(uint16_t port);
void absoluteOutL(uint16_t port, uint32_t data);

void ioWait(); // Wait for an I/O operation to complete.

#endif // ABS_IO_H
