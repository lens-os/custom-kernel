// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "IORange.h"
#include <io/absoluteio.h>
#define DEBUG true
#include <debug.h>
#include <printf/printf.h>

bool debugIO = false;

char IORange::inB(uint16_t offset) {
  if (debugIO) {
    debugPrint("inB()\n");
    break();
  }
  return absoluteInB(this->base + offset);
}

void IORange::outB(uint16_t offset, char data) {
  if (debugIO) {
    debugPrint("outB()\n");
    break();
  }
  absoluteOutB(this->base + offset, data);
}

uint16_t IORange::inW(uint16_t offset) {
  if (debugIO) {
    debugPrint("inW()\n");
    break();
  }
  return absoluteInW(this->base + offset);
}

void IORange::outW(uint16_t offset, uint16_t data) {
  if (debugIO) {
    debugPrint("outW()\n");
    break();
  }
  absoluteOutW(this->base + offset, data);
}

uint32_t IORange::inL(uint16_t offset) {
  if (debugIO) {
    debugPrint("inL()\n");
    break();
  }
  return absoluteInL(this->base + offset);
}

void IORange::outL(uint16_t offset, uint32_t data) {
  if (debugIO) {
    debugPrint("outL()\n");
    break();
  }
  absoluteOutL(this->base + offset, data);
}

void IORange::ioWait() {
  if (debugIO) {
    debugPrint("ioWait()\n");
    break();
  }
  absoluteOutB(0xFFFF, 0x00);
}

IORange::IORange(uint16_t basePort) {
  this->base = basePort;
}

IORange::IORange() {
  this->base = 0x0000;
}
