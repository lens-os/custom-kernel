// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "absoluteio.h"

/**
 * @file io/absoluteio.cpp
 *
 * Absolute I/O functions.
 *
 * Not remotely portable.
 */

// Inputs one byte from an absolute port.
char absoluteInB(uint16_t port) {
  char result;
  asm("inb %%dx, %%al" : "=a" (result) : "d" (port));
  return result;
}

// Outputs one byte to an absolute port.
void absoluteOutB(uint16_t port, char data) {
  asm("outb %%al, %%dx" : :"a" (data), "d" (port));
}

// Inputs one word from an absolute port.
uint16_t absoluteInW(uint16_t port) {
  uint16_t result;
  asm("inw %%dx, %%ax" : "=a" (result) : "d" (port));
  return result;
}

// Outputs one word to an absolute port.
void absoluteOutW(uint16_t port, uint16_t data) {
  asm("outw %%ax, %%dx" : :"a" (data), "d" (port));
}


// Inputs one longword from an absolute port.
uint32_t absoluteInL(uint16_t port) {
  uint32_t result;
  //asm("inl %%edx, %%eax" : "=a" (result) : "d" (port));
  return result;
}

// Outputs one longword to an absolute port.
void absoluteOutL(uint16_t port, uint32_t data) {
  //asm("outl %%eax, %%edx" : :"a" (data), "d" (port));
}


 // Writes out to an unused port, to waste one I/O cycle.
void ioWait() {
  absoluteOutB(0xFFFF, 0x00);
}
