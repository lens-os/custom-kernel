// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file buildinfo.h
 *
 * Information about the build of the kernel.
 */

extern const char *COMPILER;
extern const char *COMPILER_VERSION;

#if __PIC__ == 1 || __PIC == 2
  #define POSITION_INDEP = true

#else
  #define POSITION_INDEP = false

#endif

extern const char *KERNEL_VERSION;
extern const char *KERNEL_PLATFORM;
