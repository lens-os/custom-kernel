// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MemoryException.h"
#include <printf/printf.h>

MemoryException::MemoryException(int code, int operation, size_t numFrames, void *block): code(code), operation(operation), numFrames(numFrames), block(block) {}

void MemoryException::asString(char *buf) {
  const char *err;
  switch (this->code) {
    case ERR_OUT_OF_MEMORY:
    err = "there was no memory available or the memory manager wasn't initialized";
    break;
    case ERR_BLOCKS_TOO_SMALL:
    err = "there was no block large enough";
    break;
    case ERR_NO_LOCK:
    err = "failed to acquire the memory management lock";
    break;
    default:
    err = "unknown error";
  }
  const char *op;
  switch (this->operation) {
    case OP_ALLOC_FRAMES:
    op = "allocating";
    break;
    case OP_ADD_FRAMES:
    op = "adding or releasing";
    break;
    default:
    op = "performing an unknown operation on";
  }
  if (this->block != NULL) {
    sprintf(buf, "Memory management error while %s %d frame(s) at 0x%lx: %d (%s).", op, this->numFrames, (long) this->block, this->code, err);
  } else {
    sprintf(buf, "Memory management error while %s %d frame(s): %d (%s).", op, this->numFrames, this->code, err);
  }
}
