// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "physmemmgr.h"
#include "MemoryException.h"
#include <sync/sync.h>
#include <kexcept/kexcept.h>

namespace PhysicalMemoryManager {

static Lock memLock = LOCK_INIT;
static MemoryBlockHeader *firstMemBlock = NULL;

/**
 * Allocates a block of page frames.
 *
 * @param size The number of frames to allocate.
 * @param error A pointer to a buffer for the error code.
 *
 * @throw MemoryException
 *
 * @return The address of the first byte of the first frame in the allocated block.
 */
void *allocateFrames(size_t size) {
  if (!acquireLock(&memLock, 3)) {
    MemoryException *exc = new MemoryException(ERR_NO_LOCK, OP_ALLOC_FRAMES, size, NULL);
    KExcept::throwExc(exc);
    return NULL; // Just in case.
  }
  if (firstMemBlock == NULL) {
    releaseLock(&memLock);
    MemoryException *exc = new MemoryException(ERR_OUT_OF_MEMORY, OP_ALLOC_FRAMES, size, NULL);
    KExcept::throwExc(exc);
    return NULL; // Just in case.
  }
  MemoryBlockHeader *last = NULL;
  for (MemoryBlockHeader *block = firstMemBlock; block != NULL; block = block->next) { // Iterate over all free memory blocks.
    if (block->size >= size) {
      if (block->size > size) {
        MemoryBlockHeader *newBlockHeader = (MemoryBlockHeader *) block + size * PAGE_SIZE; // Create a new header for the remaining part of the block.
        *newBlockHeader = {block->size - size, block->next};
        if (last != NULL) { // And substitute it for the old header.
          last->next = newBlockHeader;
        } else {
          firstMemBlock = newBlockHeader;
        }
      } else { // We're using the entire block.
        last->next = block->next; // Just remove the block from the list entirely.
      }
      block->size = 0; // Zero the header and return the block.
      block->next = NULL;
      releaseLock(&memLock);
      return block;
    }
    last = block;
  }
  releaseLock(&memLock);
  MemoryException *exc = new MemoryException(ERR_BLOCKS_TOO_SMALL, OP_ALLOC_FRAMES, size, NULL);
  KExcept::throwExc(exc);
  return NULL;
}

/**
 * Adds a block of page frames to the free list.
 *
 * @param block A pointer to the first byte of the block.
 * @param size The number of frames in the block
 *
 * @return 0 for success.
 */
int addFrames(void *block, size_t size) {
  if (!acquireLock(&memLock, 3)) {
    return 1; // Error: Could not acquire memory manager lock.
  }
  MemoryBlockHeader *freed = (MemoryBlockHeader *) block;
  if (firstMemBlock == NULL) {
    freed->next = NULL; // Insert the block at the beginning.
    freed->size = size;
    firstMemBlock = freed;
    releaseLock(&memLock);
    return 0;
  }

  MemoryBlockHeader *last = NULL;
  for (MemoryBlockHeader *block = firstMemBlock; block != NULL; block = block->next) { // Iterate over all free memory blocks.
    if (last < freed && freed < block) { // Find the blocks between which freed fits.
      freed->next = block; // Insert the block.
      freed->size = size;
      last->next = freed;
      break;
    }
    last = block;
  }

  // Combine adjacent blocks.
  last = NULL;
  bool changed = false;
  for (MemoryBlockHeader *block = firstMemBlock; block != NULL; block = block->next) { // Iterate over all free memory blocks.
    if (last != NULL && (last + last->size * PAGE_SIZE == block)) { // Two blocks are adjacent.
      last->next = block->next; // Combine them.
      last->size = last->size + block->size;
      changed = true;
    }
    if (block->next == NULL) { // We've reached the end.
      if (!changed) { // If we didn't change anything, we're done.
        break;
      }
      block = firstMemBlock; // Otherwise, wrap around.
      changed = false;
    }
    last = block;
  }
  releaseLock(&memLock);
  return 0;
}

}
