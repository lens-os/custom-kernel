# Changelog

## Version 0.01 (targets)

* [X] VGA color text output
* [ ] PS/2 keyboard input
* [X] Dynamic memory allocation
* [X] Exception handling
* [X] Basic Unicode support
* [ ] Kernel-mode command processor
* [ ] Respond to at least two distinct commands
* [X] Building and style guides
