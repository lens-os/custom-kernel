# Contributing to LENS

To contribute to LENS, follow these steps:

1. Fork the project.
2. Make your changes on a separate branch (branched from develop) named for the feature or fix (e.g. feature/improve-interrupt-dispatch or hotfix/fix-scheduler-panic).
3. Submit a merge request.
4. Wait for the review process to complete.
5. Make any requested changes.
6. Repeat from #4 as many times as needed.
7. Congrats!

Make sure you build and test the relevant components before submitting the request.
