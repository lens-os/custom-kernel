# Style guide

## Spacing and braces

LENS code should be indented using 2-wide soft tabs, like so:

```cpp
{
  // Something here
}
```

Braces should be on the same line as the statement or definition they are attached to, with a space, like so:

```cpp
void something() {
  // Code here
}
```

Argument lists should have no spacing except after commas, like so:

```cpp
something(int arg1, int arg2);
```

## Naming

Names should always be descriptive, but not excessively long.

Functions and variables (whether local, parameter, global, or member) in LENS should be named in camelCase, like so:

```cpp
int exampleVariable;

void exampleFunction();
```

Classes, structs, unions, and enums should be named in PascalCase:

```cpp
class ExampleClass {
  int aMember;
};
```

Structs, unions, and enums should be typedefed, and enum values should be camelCased:

```cpp
typedef struct ExampleStruct {
  int aMember;
} ExampleStruct;

typedef union ExampleUnion {
  int aMember;
  long anotherMember;
} ExampleUnion;

typedef enum ExampleEnum {
  aValue;
  anotherValue;
} ExampleEnum
```

Names should fully capitalize acronyms unless at the start of a camelCase name, like so:

```cpp
VGAController *mainVGA;
PS2Keyboard *ps2Keyboard;
```

These rules do not apply to names from external specifications, e.g.:

* `malloc`: From the ISO C standard. Would be `allocateMemory` in LENS style.
* `time_t`: From the ISO C standard. Would be `Time` in LENS style.

### File naming

Files containing class definitions should be named based on the file they contain, e.g. `ExampleClass.cpp` and `ExampleClass.h`. Other files should be named descriptively, briefly, and in an all-lowercase style (e.g. `devicemgr.cpp`, `threadstate.h`).

## Documentation

Documentation comments should be placed at the definition of any class or complex function (and at the declarations of all virtual ... = 0 functions), in Doxygen's "autobrief JavaDoc style." Thrown kexcept exceptions should be documented. Each public and protected member of a class should have a `///<`-style comment describing it (private members should have regular comments). For example:

```cpp
/**
 * Performs a complete enumeration of the bus.
 *
 * Retrieves all device types and IDs and starts drivers for known devices.
 *
 * @param mode The mode to enumerate in.
 * @param callback A callback to invoke for each device found. If NULL, no callback is invoked.
 * @return The number of devices discovered.
 */
int Bus::enumerate(BusEnumMode mode) {
  // Code here
}

/**
 * A class.
 *
 * A class with documentation for its properties.
 */
class ComplexClass {
public:
  int aMember; ///< A member variable.
};
```
