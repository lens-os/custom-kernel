; LENS operating system kernel copyright (C) 2018 Alexander Martin.

; This file is part of LENS.

; LENS is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; LENS is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with LENS.  If not, see <http://www.gnu.org/licenses/>.

bits 64

global start64
extern stack_bottom
extern kernelMain

section .text

start64:
  mov rsp, stack_bottom ; Reset stack in 64-bit mode, discarding anything on the stack.

  mov dx, 0
  mov ss, dx
  mov ds, dx
  mov es, dx
  mov fs, dx
  mov gs, dx

  mov rsi, rax ; Pass RAX (the bootloader magic number) to kernel_main.
  mov rdi, rbx ; Pass RBX (a pointer to the multiboot_info structure) to kernel_main.

  call kernelMain ; Call kernel_main

.halt:
  cli ; Halt the system.
.halt.lop:
  hlt
  jmp .halt.lop
