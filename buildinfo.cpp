// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file buildinfo.cpp
 *
 * Information about the build of the kernel.
 */

// Which compiler?
#if defined(__clang__)
  const char _COMPILER[11] = "Clang/LLVM";

#elif defined(__ICC) || defined(__INTEL_COMPILER)
  const char _COMPILER[15] = "Intel ICC/ICPC";

#elif (defined(__GNUC__) || defined(__GNUG__)) && !(defined(__clang__) || defined(__INTEL_COMPILER))
  const char _COMPILER[12] = "GNU GCC/G++";

#elif defined(__HP_cc) || defined(__HP_aCC)
  const char _COMPILER[23] = "Hewlett-Packard C/aC++";

#elif defined(__IBMC__) || defined(__IBMCPP__)
  const char _COMPILER[13] = "IBM XL C/C++";

#elif defined(_MSC_VER)
  const char _COMPILER[24] = "Microsoft Visual Studio";

#elif defined(__PGI)
  const char _COMPILER[26] = "Portland Group PGCC/PGCPP";

#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
  const char _COMPILER[22] = "Oracle Solaris Studio";

#elif (defined(__GNUC__) || defined(__GNUG__)) && (defined(__clang__) || defined(__INTEL_COMPILER))
  const char _COMPILER[35] = "an unknown GCC-compatible compiler";

#else
  const char _COMPILER[17] = "an unknown compiler";

#endif

const char *COMPILER = (const char *) &_COMPILER;

// Version.
#ifdef __VERSION__
  const char _COMPILER_VERSION[64] = __VERSION__;

#else
  const char _COMPILER_VERSION[8] = "unknown";

#endif

const char *COMPILER_VERSION = (const char *) &_COMPILER_VERSION;

const char _KERNEL_VERSION[21] = "0.01 (working build)";
const char _KERNEL_PLATFORM[14] = "PC-Compatible";

const char *KERNEL_VERSION = (const char *) &_KERNEL_VERSION;
const char *KERNEL_PLATFORM = (const char *) &_KERNEL_PLATFORM;
