@numchapentry{Introduction to Multiboot Specification}{1}{Overview}{1}
@numsecentry{The background of Multiboot Specification}{1.1}{Motivation}{1}
@numsecentry{The target architecture}{1.2}{Architecture}{1}
@numsecentry{The target operating systems}{1.3}{Operating systems}{1}
@numsecentry{Boot sources}{1.4}{Boot sources}{1}
@numsecentry{Configure an operating system at boot-time}{1.5}{Boot-time configuration}{2}
@numsecentry{How to make OS development easier}{1.6}{Convenience to operating systems}{2}
@numsecentry{Boot modules}{1.7}{Boot modules}{2}
@numchapentry{The definitions of terms used through the specification}{2}{Terminology}{4}
@numchapentry{The exact definitions of Multiboot Specification}{3}{Specification}{5}
@numsecentry{OS image format}{3.1}{OS image format}{5}
@numsubsecentry{The layout of Multiboot header}{3.1.1}{Header layout}{5}
@numsubsecentry{The magic fields of Multiboot header}{3.1.2}{Header magic fields}{5}
@numsubsecentry{General tag structure}{3.1.3}{Header tags}{6}
@numsubsecentry{Multiboot information request}{3.1.4}{Information request header tag}{6}
@numsubsecentry{The address tag of Multiboot header}{3.1.5}{Address header tag}{6}
@numsubsecentry{The entry address tag of Multiboot header}{3.1.6}{}{7}
@numsubsecentry{Flags tag}{3.1.7}{Console header tags}{7}
@numsubsecentry{The framebuffer tag of Multiboot header}{3.1.8}{}{8}
@numsubsecentry{Module alignment tag}{3.1.9}{Module alignment tag}{8}
@numsubsecentry{EFI boot services}{3.1.10}{EFI boot services}{8}
@numsecentry{MIPS machine state}{3.2}{Machine state}{9}
@numsecentry{I386 machine state}{3.3}{}{9}
@numsecentry{Boot information}{3.4}{Boot information format}{10}
@numsubsecentry{Boot information format}{3.4.1}{}{10}
@numsubsecentry{Basic tags structure}{3.4.2}{}{10}
@numsubsecentry{Basic memory information}{3.4.3}{}{10}
@numsubsecentry{BIOS Boot device}{3.4.4}{}{11}
@numsubsecentry{Boot command line}{3.4.5}{}{11}
@numsubsecentry{Modules}{3.4.6}{}{12}
@numsubsecentry{ELF-Symbols}{3.4.7}{}{12}
@numsubsecentry{Memory map}{3.4.8}{}{12}
@numsubsecentry{Boot loader name}{3.4.9}{}{13}
@numsubsecentry{APM table}{3.4.10}{}{13}
@numsubsecentry{VBE info}{3.4.11}{}{14}
@numsubsecentry{Framebuffer info}{3.4.12}{}{14}
@numsubsecentry{EFI 32-bit system table pointer}{3.4.13}{}{15}
@numsubsecentry{EFI 64-bit system table pointer}{3.4.14}{}{16}
@numsubsecentry{SMBIOS tables}{3.4.15}{}{16}
@numsubsecentry{ACPI old RSDP}{3.4.16}{}{16}
@numsubsecentry{ACPI new RSDP}{3.4.17}{}{16}
@numsubsecentry{Networking information}{3.4.18}{}{16}
@numsubsecentry{EFI memory map}{3.4.19}{}{17}
@numsubsecentry{EFI boot services not terminated}{3.4.20}{}{17}
@numchapentry{Examples}{4}{Examples}{18}
@numsecentry{Notes on PC}{4.1}{Notes on PC}{18}
@numsecentry{BIOS device mapping techniques}{4.2}{BIOS device mapping techniques}{18}
@numsubsecentry{Data comparison technique}{4.2.1}{Data comparison technique}{18}
@numsubsecentry{I/O restriction technique}{4.2.2}{I/O restriction technique}{19}
@numsecentry{Example OS code}{4.3}{Example OS code}{19}
@numsubsecentry{multiboot2.h}{4.3.1}{multiboot2.h}{19}
@numsubsecentry{boot.S}{4.3.2}{boot.S}{28}
@numsubsecentry{kernel.c}{4.3.3}{kernel.c}{30}
@numsubsecentry{Other Multiboot kernels}{4.3.4}{Other Multiboot kernels}{38}
@numsecentry{Example boot loader code}{4.4}{Example boot loader code}{38}
@numchapentry{The change log of this specification}{5}{History}{39}
@unnchapentry{Index}{10001}{Index}{40}
