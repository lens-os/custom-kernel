// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <types.h>
#include <drivers/drivers.h>
#include <mem/physmemmgr.h>
#include <util/util.h>
#include <util/strutil.h>
#include <multiboot.h>
#include <interrupts/interruptmgr.h>
#include <io/IORange.h>
#define DEBUG true
#include <debug.h>

/**
 * @file kernel.cpp
 *
 * Contains the kernel main function.
 */

VGAController mainVGA;
#ifdef DRIVER_PS2_CONTROLLER
PS2Controller defaultPs2Controller;
#endif
#ifdef DRIVER_SERIALPORT
SerialPort serialPort1;
#endif
I8259PIC *intController;
bool IRQHappened = false;

__attribute__((interrupt)) void testSyscallHandler(InterruptManager::interruptFrame *frame) {
  UNUSED_PARAM(frame);
  printk("System call.\n");
}

__attribute((interrupt)) void testIRQHandler(InterruptManager::interruptFrame *frame) {
  UNUSED_PARAM(frame);
  printk("IRQ 1!\n");
  IRQHappened = true;
}

__attribute__((interrupt)) void shimDoubleFaultHandler(InterruptManager::interruptFrame *frame, uint64_t errorCode) {
  UNUSED_PARAM(frame);
  UNUSED_PARAM(errorCode);
  panic("Double fault.");
}

__attribute__((interrupt)) void shimGeneralProtectionFaultHandler(InterruptManager::interruptFrame *frame, uint64_t errorCode) {
  UNUSED_PARAM(frame);
  UNUSED_PARAM(errorCode);
  panic("General protection fault.");
}

/**
 * The kernel's main entry point.
 *
 * Called by the boot assembly code once in 64-bit mode.
 *
 * @param mbootInfo A pointer to the Multiboot info structure passed by the bootloader.
 * @param magic The Multiboot magic number.
 */
extern "C" void kernelMain(multiboot_info_t *mbootInfo, uint64_t magic) {
  PhysicalMemoryManager::addFrames((void *) 0x1000, 159); // Give Conventional Memory (*gasp*) to the physical memory manager as a kludge (it's definitely there).
  mainVGA = VGAController();
  mainVGA.start();
  // defaultPs2Controller = PS2Controller();
  serialPort1 = SerialPort(1);
  printk("This is the "); // Prefix to "LENS kernel version..."
  printVersion();
  printk("LENS operating system kernel copyright (C) 2015 Alexander Logan Martin. Some rights reserved.\n");
  printf("Multiboot magic number is: 0x%lx.\n", (long) magic);
  printf("Multiboot info is at address: 0x%lx.\n", (long) mbootInfo);
  // halt();
  InterruptManager::init(); // Initialize the interrupt manager.
  InterruptManager::ISR handler;
  handler.intr = (InterruptManager::InterruptHandler) &testSyscallHandler;
  InterruptManager::setInterruptHandler(0x40, handler, false, true, 0);
  intController = new I8259PIC();
  handler.intr = (InterruptManager::InterruptHandler) &testIRQHandler;
  InterruptManager::setInterruptHandler(0x51, handler, true, false, 0);
  intController->start();
  intController->mapIRQs(0x50);
  intController->maskIRQ(1, true);
  while (!IRQHappened) {} // Await the IRQ.
  // defaultPs2Controller.start(); // Start the PS/2 controller driver.
  /*serialPort1.setBaud(rate_115200);
  serialPort1.setEncoding(DATA_8, STOP_1, PARITY_NONE);
  serialPort1.start();
  serialPort1.write("Test.", 5);*/
  /*while (true) {
    char b = 0;
    serialPort1.read(&b);
    serialPort1.write(&b, 1);
  }*/
  // halt();
  printk("Testing fault handling...\n");
  break();
  int test = 1/0;
  printf("1/0 = %d.", test);
  halt();
}
