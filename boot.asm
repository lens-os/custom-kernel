; LENS operating system kernel copyright (C) 2018 Alexander Martin.

; This file is part of LENS.

; LENS is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; LENS is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with LENS.  If not, see <http://www.gnu.org/licenses/>.

; Some various boot-time constants.
section .rodata

global gdt64_codeSegment

gdt64:
dq 0 ; NULL segment.
gdt64_codeSegment: equ $ - gdt64
dq (1<<43) | (1<<44) | (1<<47) | (1<<53) ; Code segment.
gdtr64:
dw 15
dq gdt64

; Create space for a stack.
section .bss

global kernel_start
global stack_bottom
global stack_top

; Clever 'design': if the stack overflows, it will overwrite the GDT first, causing a GPF and 'alerting us' to the problem. :)

kernel_start:
align 4
stack_top:
  resb 32768 ; Reserve space for a 32K stack in bss.
stack_bottom:

page_table:
align 4096
pl4_table: ; Paging Level 4 Table.
  resb 4096
pl3_table: ; Paging Level 3 Table.
  resb 4096
pl2_table: ; Paging Level 2 Table.
  resb 4096
pl1_table: ; Paging Level 1 Table.
  resb 4096

section .text

bits 32

global _start
global setupIdentityPaging
global enablePaging
global enterLongMode
extern start64

; Start symbol.
_start:
  mov esp, stack_bottom ; Set up stack.
  push eax
  push ebx

  call enterLongMode

.err:
  mov word [0xb8000], 0x4f45
  cli
.errLoop:
  hlt
  jmp .errLoop


; setupIdentityPaging and enablePaging are almost entirely from https://os.phil-opp.com/entering-longmode.html. Thanks, Phil!

setupIdentityPaging:
  push eax
  push ecx

  mov eax, pl3_table ; Map first PL4 entry to PL3 table.
  or eax, 11b ; Present + writable.
  mov [pl4_table], eax

  mov eax, pl2_table ; Map first PL3 entry to PL2 table.
  or eax, 11b ; Present + writable.
  mov [pl3_table], eax

  mov ecx, 0

.mapPL2Loop: ; Loop to actually map the PL2 table.
  mov eax, 0x200000  ; 2MiB
  mul ecx            ; Start address of page.
  or eax, 10000011b ; Present + writable + huge.
  mov [pl2_table + ecx * 8], eax ; Map entry.

  inc ecx
  cmp ecx, 512
  jl .mapPL2Loop

.return:
  pop ecx
  pop eax
  ret

enablePaging:
  push eax
  push ecx

  mov eax, pl4_table ; Load CR3 with the PL4 table, to tell the CPU where to find the page table.
  mov cr3, eax

  mov eax, cr4 ; Enable the PAE flag in CR4
  or eax, 1 << 5
  mov cr4, eax

  mov ecx, 0xC0000080 ; Set the long mode flag in MSR 0xC0000080.
  rdmsr
  or eax, 1 << 8
  wrmsr

  mov eax, cr0 ; Actually enable paging.
  or eax, 1 << 31
  mov cr0, eax

  pop ecx
  pop eax
  ret

enterLongMode:
  call setupIdentityPaging
  call enablePaging

  lgdt [gdtr64]

  pop ecx ; Remove return address and then restore multiboot magic/info before going to start64.
  pop ebx
  pop ecx
  jmp gdt64_codeSegment:start64
