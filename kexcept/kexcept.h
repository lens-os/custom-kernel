// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEXCEPT_H
#define KEXCEPT_H

#include "Exception.h"
#include <util/threadstate.h>

namespace KExcept {

/**
 * @namespace KExcept
 *
 * Basic kernel-space exception support.
 *
 * Based on a longjmp-like mechanism and catches exceptions through a catching call.
 */

typedef void *(*catchableFunction)(void *);

typedef struct CatchResult {
  Exception *caught;
  void *result;
} CatchResult;

typedef struct State {
  Exception *currentException;
  ThreadState *currentCatchState;
} State;

/*
 * !!BIG WARNING!!
 *
 * throwExc() *does not* unwind the stack; as such, it doesn't call any destructors or release any memory!
 * If you call a function that throws an exception and you have objects that must be destroyed or memory that must be released, you *must* set a catch point to do this yourself!
 * To facilitate this, please note functions that throw an exception, or that call throwing functions without catching, with a comment.
 */

CatchResult catchCall(catchableFunction func, void *param);
void throwExc(Exception *exc);

}

#endif // KEXCEPT_H
