// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "utf8.h"
#include <util/util.h>
namespace Unicode {

/**
 * Interprets a UTF-8 character.
 *
 * @param bytes A pointer to the first byte of the character.
 * @param buf A pointer to a buffer for the codepoint.
 * @param lenBuf A pointer to a buffer for the length of the character.
 *
 * @return true for success, false for a bad character.
 */
bool parseUtf8Character(char *bytes, codepoint *buf, int *lenBuf) { // Interprets a UTF-8 character, putting the codepoint in buf and the size in lenBuf. Returns false for bad char.
  if ((bytes[0] & 0xc0) == 0xc0) { // If it starts with 11...
    int numBytes = 2; // Set the number of bytes to two...
    for (int i = 5; i > 2; i--) { // Loop through all the UTF-8 special bits...
      if (bytes[0] & (1 << i)) { // Unintutitive bitwise/shifting check if the bit is set.
        numBytes++; // If it is, increment the number of bytes in the character.
      } else {
        break; // If it's not, we're done with the special bits.
      }
    }
    *lenBuf = numBytes;
    for (int i = 0; i <= numBytes; i++) { // Loop through all the bytes...
      char thisByte = bytes[i]; // Put each in a variable...
      if (i == 0) { // If this is the first byte...
        for (i = 7; 7 - i <= numBytes; i--) { // Zero the special bits...
          thisByte = thisByte & (255 - (2^i));
        }
        *buf = thisByte; // And put the byte into the buffer.
      } else { // If this isn't the first byte...
        if (thisByte & 0x80) { // If it starts with 10...
          *buf = *buf << 6; // Shift existing data in the buffer...
          *buf = *buf | (thisByte & 0x3F); // And put the data bits (zeroing the special bits) in.
        } else { // If it doesn't start with 10...
          return false; // Failure!
        }
      }
    }
    return true;
  } else { // If it doesn't start with 11...
    *buf = (codepoint) bytes[0]; // 1-byte character can just be returned as-is.
    *lenBuf = 1;
    return true; // Success!
  }
}

}
