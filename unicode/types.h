// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UNICODE_TYPES_H
#define UNICODE_TYPES_H

#include <types.h>
namespace Unicode {

/**
 * @namespace Unicode
 *
 * Unicode implementation.
 */

/**
 * @file unicode/types.h
 *
 * Types used by the Unicode implementation.
 */

typedef uint32_t codepoint;

typedef struct GlyphAssoc { // Association between a block of Unicode code points and glyph #s in console fonts.
  codepoint firstPoint;
  char firstGlyph; // Note that more than one code point can reference a given glyph.
  int numAssocs;
  GlyphAssoc *next; // Forms a linked list with other GlyphAssocs.
} GlyphAssoc;

typedef struct Glyph { // Because C(++) acts weird when presented with typedeffed arrays.
  char v[14];
} glyph;

typedef struct FontGlyphBlock { // Block of 128 glyphs.
  glyph glfs[128];
  FontGlyphBlock *next;
} FontGlyphBlock;

}

#endif // UNICODE_TYPES_H
