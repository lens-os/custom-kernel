// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Font.h"
#include <util/util.h>
namespace Unicode {

bool Font::getGlyph(char glyphNum, glyph *buf) {
  if (glyphNum >= this->numGlyphs) {
    return false;
  }
  FontGlyphBlock *currentBlock;
  for (int i = 0; i * 128 < glyphNum; i++) { // Find the block containing the glyph.
    currentBlock = currentBlock->next;
  }
  memcpy(&buf[0], &(currentBlock->glfs[glyphNum % 128]), sizeof(Glyph)); // Copy the correct glyph from the block.
  return true;
}

char Font::getGlyphNumForCodePoint(codepoint point) {
  for (GlyphAssoc *currentAssoc = this->firstGlyphAssoc; currentAssoc->next != NULL; currentAssoc = currentAssoc->next) { // Walk the list.
    if (currentAssoc->firstPoint < point || currentAssoc->firstPoint + currentAssoc->numAssocs > point) { // Check if we've found the point.
      return currentAssoc->firstGlyph + (point - currentAssoc->firstPoint); // Return the correct glyph.
    }
  }
  return 0;
}

bool Font::getGlyphForCodePoint(codepoint point, glyph *buf) {
  return this->getGlyph(this->getGlyphNumForCodePoint(point), buf);
}

}
